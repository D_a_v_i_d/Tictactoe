"""
From: https://www.neverstopbuilding.com/blog/minimax
A description for the algorithm, assuming X is the "turn taking player," would look something like:

If the game is over, return the score from X's perspective.
Otherwise get a list of new game states for every possible move
Create a scores list
For each of these states add the minimax result of that state to the scores list
If it's X's turn, return the maximum score from the scores list
If it's O's turn, return the minimum score from the scores list
"""
from .gamestate import GameState

class Node:
    pass

class AI:
    pass

    